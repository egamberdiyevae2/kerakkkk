import React from 'react'

function NextButton({ answer, dispatch, index, numberOfQuestions }) {
  if (answer == null) {
    return null
  } else if(index+1==numberOfQuestions) {
    return <button
      className='btn btn-ui'
      onClick={() => dispatch({ type: "finished" })}>
      Finish
    </button>
  }
  else  {
    return (<button
      className='btn btn-ui'
      onClick={() => dispatch({ type: "nextQuestions" })}>
      Next
    </button>)
  }
}

export default NextButton