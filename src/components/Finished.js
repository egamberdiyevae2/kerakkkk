import React from 'react'

const Finished = ({point}) => {
  return (
    <h1>Finished with {point}</h1>
  )
}

export default Finished