// import React, { useEffect, useReducer, useState } from "react";
// import Header from "./Header";
// import Main from "./Main";
// import Loader from "./Loader"
// import Error from "./Error";
// import StartScreen from "./startScreen";
// import Questions from "./Questions";
// const initialState = {
//   questions: [],
//   status: "loading",
//   index: 0,
//   point: 0,
//   answer: null
// }
// const reducer = (state, action) => {
//   switch (action.type) {
//     case "dataReceive":
//       return {
//         ...state,
//         questions: action.payload,
//         status: "ready"
//       };
//     case "error":
//       return {
//         ...state,
//         status: "error"
//       };
//     case "start":
//       return {
//         ...state,
//         status: "active"
//       };
//     case "newAnswer":
//       const questions = state.at(action.payload)
//       return {
//         ...state,
//         answer: action.payload,
//         point: action.payload == questions.correctOption ? (state.point + questions.point) : (state.point + 0),
//         index: state.index + 1
//       };
//     default: throw new Error("This action in undefined");
//   }
// }
// const App = () => {
//   const [{ questions, status, index, point, answer }, dispatch] = useReducer(reducer, initialState);
//   // const [answer, setAnswer] = useState(0);
//   const numberOfQuestions = questions.length;
//   useEffect(() => {
//     fetch("http://localhost:8080/questions")
//       .then((res) => res.json())
//       .then((data) => dispatch({ type: "dataReceive", payload: data }))
//       .catch((err) => dispatch({ type: "error" }))
//   }, [])
//   return (
//     <div className="app">
//       <Header />
//       <Main>
//         {
//           status == "loading" && <Loader />
//         }
//         {
//           status == "error" && <Error />
//         }
//         {
//           status == "ready" && <StartScreen dispatch={dispatch} questions={numberOfQuestions} />
//         }
//         {
//           status == "active" && <Questions questions={questions[index]} dispatch={dispatch} answer={answer} />
//         }
//         {/* <h1>QUESTIONS : {state.questions.length}</h1> */}
//         {/* <h1>QUESTIONS : {answer}</h1> */}
//         {/* <p>question 1 / 15</p> */}
//       </Main>
//     </div>
//   );
// };

// export default App;

import React, { useEffect, useReducer, useState } from "react";
import Header from "./Header";
import Main from "./Main";
import Loader from "./Loader";
import Error from "./Error";
import StartScreen from "./StartScreen";
import Questions from "./Questions";
import NextButton from "./NextButton";
import Finished from "./Finished";
//*backend dan keladigan ma'lumotni saqlaydigan qism
const initialState = {
  questions: [],
  // ready,error,finished,next,active
  status: "loading",
  index: 0,
  point: 0,
  answer: null,
};
//*reducer funksiyasi 2 ta parametr qabul qiladi 1-si usereducer dagi state 2-si dispatchdan keladigan qiymatlar objecti
function reducer(state, action) {
  switch (action.type) {
    case "dataReceive":
      return {
        ...state,
        questions: action.payload,
        status: "ready",
      };
    case "error":
      return {
        ...state,
        questions: action.payload,
        status: "error",
      };
    case "start":
      return {
        ...state,
        status: "active",
      };
    case "newAnswer":
      const question = state.questions.at(state.index);
      console.log(question);
      console.log(action.payload);
      return {
        ...state,
        answer: action.payload,
        point:
          action.payload == question.correctOption
            ? state.point + question.points
            : state.point,
      };
    case "nextQuestions":
      return {
        ...state,
        index: state.index + 1,
        answer: null,
      };
    case "finished":
      return {
        ...state,
        status: "finish",
      };
    default:
      throw new Error("This action is undefined");
  }
}
const App = () => {
  //*initialState bu dispatchning argumenti yani object
  //*state ga objectlar keladi shuning uchun object destruction qilib yozamiz :
  // const [state,dispatch]=useReducer(reducer,initialState)
  const [{ questions, status, index, point, answer }, dispatch] = useReducer(
    reducer,
    initialState
  );
  const numberOfQuestions = questions.length;
  useEffect(() => {
    fetch("http://localhost:8080/questions")
      .then((res) => res.json())
      .then((data) => dispatch({ type: "dataReceive", payload: data }))
      .catch((err) => dispatch({ type: "error", payload: err.massage }));
  }, []);

  return (
    <div className="app">
      <Header />
      <Main>
        {status == "loading" && <Loader />}
        {status == "error" && <Error />}
        {status == "ready" && (
          <StartScreen questions={numberOfQuestions} dispatch={dispatch} />
        )}
        {status == "active" && (
          <>
            <Questions
              questions={questions[index]}
              dispatch={dispatch}
              answer={answer}
            />
            <NextButton
              dispatch={dispatch}
              index={index}
              numberOfQuestions={numberOfQuestions}
              answer={answer}
            />
          </>
        )}
        {status == "finish" && <Finished point={point} />}
      </Main>
    </div>
  );
};

export default App;
